let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}



//correct
function enqueue(item){
    // add an item
    collection.push(item);
    return collection;
}

//----------------------------------------------

function dequeue(){
    // remove an item
    collection.shift();
    return collection;
    

}

// -------------------------------------------------
//correct
function front(){
    // get the front item
    return collection[0];

}

//correct
function size(){
    // get the size of the array
    return collection.length;


}

//correct
function isEmpty(){
    // check array if empty
    if (collection.length > 0) {
        return false;
    } else {
        return true;
    }


}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};